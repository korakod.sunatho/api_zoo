const express = require('express');
const mysql = require('mysql')
const bodyParser = require('body-parser');
const md5 = require('md5');
global.db = mysql.createPool({
    connectionLimit: 30,
    host: "127.0.0.1",
    user: "root",
    port: "3306",
    password: "",
    database: "apizoo"
  });
  
  //

const app = express();
const port = process.env.PORT || 4000
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

const highlight = () => {
    return new Promise((resolve) => {
        let sql1 = "SELECT highlight_name,highlight_img FROM tb_highlight "
        let query = db.query(sql1, (err, results) => {
            if (err) throw err
            resolve(results)
        })
    })
}
const allhighlight = () => {
    return new Promise((resolve) => {
        let sql1 = "SELECT highlight_name,highlight_img,highlight_detail,flag_delete FROM tb_highlight "
        let query = db.query(sql1, (err, results) => {
            if (err) throw err
            resolve(results)
        })
    })
}

const waterpark = () => {
    return new Promise((resolve) => {
        let sql1 = "SELECT waterpark_id,waterpark_name,waterpark_detail,waterpark_time,waterpark_ticketprice,waterpark_img FROM tb_waterpark "
        let query = db.query(sql1, (err, results) => {
            if (err) throw err
            resolve(results)
        })
    })
}

const sealshow = () => {
    return new Promise((resolve) => {
        let sql1 = "SELECT sealshow_id,sealshow_name,sealshow_time,sealshow_price,sealshow_img FROM tb_sealshow "
        let query = db.query(sql1, (err, results) => {
            if (err) throw err
            resolve(results)
        })
    })
}

const accom = () => {
    return new Promise((resolve) => {
        let sql1 = "SELECT accom_id,accom_name,accom_price,accom_contact,accom_pic1,accom_pic2 FROM tb_accom "
        let query = db.query(sql1, (err, results) => {
            if (err) throw err
            resolve(results)
        })
    })
}
const accomtype = (type) => {
    return new Promise((resolve) => {
        let sql1 = "SELECT accom_id,accom_name,accom_price,accom_contact,accom_pic1,accom_pic2 FROM tb_accom WHERE accom_name='"+type+"'"
        let query = db.query(sql1, (err, results) => {
            if (err) throw err
            resolve(results)
        })
    })
}

const login = (username,password) => {
    return new Promise((resolve) => {
        let sql1 = "SELECT customer_username,customer_password FROM tb_customer WHERE customer_username='"+username+"'"
        let query = db.query(sql1, (err, results) => {
            if (err) throw err

            if(results[0]===undefined){
                resolve("User is undefined")
            }
            else{
                if(results[0].customer_username===username &&results[0].password===md5(password)  ){
                    resolve("Success")
                }
                else{
                    resolve("Wrong Password")
                }

            }

            
        })
    })
}


const all_animal = () => {
    return new Promise((resolve) => {
        let sql1 = "SELECT animal_thainame,animal_engname FROM tb_animal "
        let query = db.query(sql1, (err, results) => {
            if (err) throw err
            resolve(results)
        })
    })
}

const search_animal = (name) => {
    return new Promise((resolve) => {
        let sql1 = "SELECT animal_thainame,animal_engname FROM tb_animal "
        
        let query = db.query(sql1, (err, results) => {
            if (err) throw err
            
            var list=[]
            var round=0
            for(round;round<=results.length-1;round++){
               
                var name1=results[round].animal_thainame
                var name2=results[round].animal_engname
                if(name1.includes(name)|| name2.includes(name)){
                    list.push(results[round]) 
                }
            }
         //   console.log(list)
            resolve(list)
        })
    })
}


const register_cus = (customer_name,customer_tel,customer_email,customer_username,customer_img,customer_password) => {
    return new Promise((resolve) => {
        var records = [customer_name,customer_tel,customer_email,customer_username,customer_img,customer_password];
      //  var sql = "UPDATE chatbot_profile SET user_status=?,state=? WHERE user_id=?"

        let sql1 = "INSERT INTO tb_customer (customer_name,customer_tel,customer_email,customer_username,customer_img,customer_password) VALUES ('"+customer_name+"','"+customer_tel+"', '"+customer_email+"','"+customer_username+"','"+customer_img+"', '"+customer_password+"')";
        let query = db.query(sql1,records, (err, results) => {
            if (err) throw err
            resolve(results)
        })
    })
}
const edit_cus = (customer_id,customer_name,customer_tel,customer_email,customer_username,customer_img,customer_password,flag_delete) => {
    return new Promise((resolve) => {
        var records = [customer_name,customer_tel,customer_email,customer_username,customer_img,customer_password,flag_delete,customer_id];
        var sql1 = "UPDATE tb_customer SET customer_name=?,customer_tel=?,customer_email=?,customer_username=?,customer_img=?,customer_password=?,flag_delete=? WHERE customer_id=?"

        let query = db.query(sql1,records, (err, results) => {
            if (err) throw err
            resolve(results)
        })
    })
}
const infomation = () => {
    return new Promise((resolve) => {
        let sql1 = "SELECT info_id,info_name,detail,info_address,info_img FROM tb_information "
        let query = db.query(sql1, (err, results) => {
            if (err) throw err
            resolve(results)
        })
    })
}

app.post('/all_animal',async (req, res) => {
    var data=await all_animal()

    res.send(data)
})
app.post('/highlight',async (req, res) => {
    res.send(await highlight())
})
app.post('/allhighlight',async (req, res) => {
    res.send(await allhighlight())
})
app.post('/waterpark',async (req, res) => {
    res.send(await waterpark())
})


app.post('/sealshow',async (req, res) => {

    
    res.send(await sealshow())
})
app.post('/accom',async (req, res) => {

    
    res.send(await accom())
})

app.post('/accomtype',async (req, res) => {

    
    res.send(await accomtype(req.body.type))
})
app.post('/search_animal',async (req, res) => {

    res.send(await search_animal(req.body.name))
})   
app.post('/infomation',async (req, res) => {

    
    res.send(await infomation())
}) 

app.post('/register_cus',async (req, res) => {
    res.send(await register_cus(req.body.customer_name,req.body.customer_tel,req.body.customer_email,req.body.customer_username,req.body.customer_img,md5(req.body.customer_password)))
}) 


app.post('/edit_cus',async (req, res) => {
    res.send(await edit_cus(req.body.customer_id,req.body.customer_name,req.body.customer_tel,req.body.customer_email,req.body.customer_username,req.body.customer_img,md5(req.body.customer_password)))
}) 

app.post('/login',async (req, res) => {
    res.send(await login(req.body.customer_username,req.body.customer_password))
}) 
app.listen(port, function () {
    console.log('run at port', port)
})

